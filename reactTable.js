import React, { Component } from 'react';
import axios from 'axios';


const Hero = props => (
    <tr>
        <td>{props.hero.name}</td>
    </tr>
)

export default class UserTab extends Component {

    constructor(props) {
        super(props);
        this.state = { heroes: [] };
    }

    componentDidMount() {
        axios.get('http://localhost:8080/users')
            .then(response => {
                console.log(response);
                this.setState({ heroes: response.data });
                console.log(this.state)
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    heroesList() {
        return this.state.heroes.map(function (currentHero, i) {
            return <Hero hero={currentHero} key={i} />;
        });
    }

    render() {
        return (
            <div className="UserTab">
                <h3>Heroes' List</h3>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                    <thead>
                        <tr>
                            <th>Name</th>Sona Novakova
Kralovehradecky kraj 
Pivovarské náměstí 1245
500 03 Hradec Králové
                        </tr>
                    </thead>
                    <tbody>
                        {this.heroesList()}
                    </tbody>
                </table>
            </div>
        )
    }
}
