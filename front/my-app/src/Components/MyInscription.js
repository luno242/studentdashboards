import React from 'react';
import { Button, Form, FormGroup, Label, Input, } from 'reactstrap';
import axios from 'axios';

export default class MyInscription extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nom: "",
            prenom:"",
            mail: " ",
            password: " ",
        };
        this.handleNomChange = this.handleNomChange.bind(this);
        this.handlePrenomChange = this.handlePrenomChange.bind(this);
        this.handleMailChange = this.handleMailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNomChange(event) {
        this.setState({ nom: event.target.value });
    }
    handlePrenomChange(event) {
        this.setState({ prenom: event.target.value });
    }
    handleMailChange(event) {
        this.setState({ mail: event.target.value });
    }
    handlePasswordChange(event) {
        this.setState({
            password: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const dataUser = {
            nom: this.state.nom,
            prenom: this.state.prenom,
            mail: this.state.mail,
            password: this.state.password
        };

        axios.post(`http://localhost:3300/users`, dataUser)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
    };

    render() {
        return (
            <Form onSubmit={this.handleSubmit} id='inscrip'>
                <h3>Inscription</h3>
                <FormGroup>
                    <Label for="exampleNom">Nom</Label>
                    <Input nom={this.state.value} onChange={this.handleNomChange} type="text" name="nom" id="exampleNom" placeholder="nom" />
                </FormGroup>
                <FormGroup>
                    <Label for="examplePrenom">Prenom</Label>
                    <Input prenom={this.state.value} onChange={this.handlePrenomChange} type="text" name="Prenom" id="examplePrenom" placeholder="prenom" />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleEmail">Email</Label>
                    <Input mail={this.state.value} onChange={this.handleMailChange} type="email" name="email" id="exampleEmail" placeholder="email" />
                </FormGroup>
                <FormGroup>
                    <Label for="examplePassword">Password</Label>
                    <Input password={this.state.value} onChange={this.handlePasswordChange} type="password" name="password" id="examplePassword" placeholder="password" />
                </FormGroup>
               
                <button type="submit" class="btn btn-primary">Submit</button>
            </Form>
        );
    }
}
