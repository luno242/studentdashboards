import React,{Component} from 'react';
import './App.css';
import { Switch, Route } from "react-router-dom";
import MyLogin from './Components/MyLogin';
import MyInscription from './Components/MyInscription';
import 'bootstrap/dist/css/bootstrap.css';


class App extends Component {
  render() {
    return (
      <div className="App">
      <Switch>
        <Route exact path="/" component={MyLogin} />
        <Route exact path="/inscription" component={MyInscription} />
      </Switch>
      </div>
     /* <div className="App">
        <MyLogin />
       
      </div>*/
     
    );
  }
}

export default App;
